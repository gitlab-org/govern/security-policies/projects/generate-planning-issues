require 'json'
require 'open3'

class GeneratePlanning
  GlabError = Class.new(StandardError)

  GROUP_ID = 9970
  NEW_VERSION_WARNING = 'new version of glab has been released'
  SECTIONS = [
    {
      types: ['type::feature', 'type::maintenance'],
      backend_or_frontend: 'backend'
    },
    {
      types: ['type::feature', 'type::maintenance'],
      backend_or_frontend: 'frontend'
    },
    {
      types: ['type::bug'],
      backend_or_frontend: 'backend'
    },
    {
      types: ['type::bug'],
      backend_or_frontend: 'frontend'
    }
  ].freeze

  DEFAULT_SECTIONS = %w[backend frontend].freeze

  attr_reader :milestone, :selected_sections

  def initialize(milestone, selected_sections)
    @milestone = milestone
    @selected_sections = selected_sections&.split(',') || DEFAULT_SECTIONS
  end

  def execute
    puts "### Fetching issues for milestone: #{milestone} with selected sections #{selected_sections}..."
    puts ""

    issues = fetch_issues

    puts "Completed. Found #{issues.count} issues."
    puts ""
    puts "### Generating planning..."
    puts ""
    puts "------------------------------------------------"
    puts "--  COPY BELOW AND PASTE INTO PLANNING ISSUE  --"
    puts "------------------------------------------------"
    puts ""

    SECTIONS
      .select { |section| selected_sections.include?(section[:backend_or_frontend]) }
      .each do |section|
        puts "### #{section[:types].map { |type| "~#{type}" }.join(" / ")} ~#{section[:backend_or_frontend]} focus"
        puts ""
        generate_entries_for_type(issues, section[:types], section[:backend_or_frontend])
          .each { |entry| puts entry }
        puts ""
      end

    puts ""
    puts "--- GROUPED BY ASSIGNEES ---"
    puts ""

    issues
      .reject { |issue| issue.dig('assignee', 'username').nil? }
      .group_by { |issue| issue['assignee']['username'] }
      .each do |assignee, assignee_issues|
        puts "### `@#{assignee}`"
        puts ""
        assignee_issues.sort_by { |issue| issue['labels'].include?('Deliverable') ? 0 : 1 }.each do |issue|
          labels = issue['labels']
          stretch_or_deliverable = labels.find { |label| label == 'Deliverable' || label == 'Stretch' }
          puts "- [ ] #{issue['references']['full']}+s (~#{stretch_or_deliverable})"
        end
        puts ""
      end
  end

  private

  def fetch_issues
    call_glab_api("groups/#{GROUP_ID}/issues -X GET -F 'milestone=#{milestone}' -F 'labels=group::security policies' -F 'per_page=100' -F 'state=opened'")
  end

  def call_glab_api(command, ignore_errors: false)
    puts "glab api #{command}"
    stdout, stderr, status = Open3.capture3("glab api #{command}")
    puts success?(status, stderr) ? "   ... success!" : "   ... failed (error: #{stderr.strip}) :("
    return {} if success?(status, stderr) && (stdout.nil? || stdout.empty?)
    return JSON.parse(stdout) if success?(status, stderr)

    raise GlabError, stderr unless ignore_errors
  end

  def success?(status, stderr)
    status.success? && (stderr.strip.empty? || stderr.include?(NEW_VERSION_WARNING))
  end

  def generate_entries_for_type(issues, types, backend_or_frontend)
    issues
      .select { |issue| issue['labels'].include?(backend_or_frontend) }
      .select { |issue| issue['labels'].any? { |label| types.include?(label) } }
      .sort_by { |issue| [issue['labels'].find { |label| label.start_with?('priority::') }.to_s, issue['labels'].find { |label| label.start_with?('severity::') }.to_s, issue['labels'].find { |label| label == 'Deliverable' || label == 'Stretch' }.to_s] }
      .map { |issue| generate_entry(issue) }
  end

  def generate_entry(issue)
    title = issue['title']
    labels = issue['labels']
    url = issue['web_url']
    priority_label = labels.find { |label| label.start_with?('priority::') }
    severity_label = labels.find { |label| label.start_with?('severity::') }
    stretch_or_deliverable = labels.find { |label| label == 'Deliverable' || label == 'Stretch' }

    priority_severity_information = priority_label.nil? ? '' : "~#{priority_label} / ~#{severity_label} "

    "* #{priority_severity_information}#{url}+s (~#{stretch_or_deliverable})"
  end
end

GeneratePlanning
  .new(ARGV[0], ARGV[1])
  .execute
