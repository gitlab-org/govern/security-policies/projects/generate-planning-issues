# Generate Planning Issue

This project allows you to quickly generate content needed for planning issue used by Security Policies team.

## Before generating the script

### What to do before preparing planning issue and running script below?

There are certain labels that needs to be supplied to issues to properly generate the content below.

1. Start with assigning issues to the proper milestone.
    1. Go to [Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1420731?label_name%5B%5D=group%3A%3Asecurity%20policies) and look for items from current milestone and backlog that should be assigned to planned milestone.
    1. Go to [Priorities Page](https://about.gitlab.com/direction/govern/security_policies/#priorities) to check if Epics scheduled for given milestone have issues created and assigned to planned milestone.
1. Go to [Frontend/Backend Board](https://gitlab.com/groups/gitlab-org/-/boards/5772471?milestone_title=Started&label_name%5B%5D=group%3A%3Asecurity%20policies) to assign proper label to issues. You can ignore issues created for UX team or feature flag rollout issues.
1. Go to [Deliverable/Stretch Board](https://gitlab.com/groups/gitlab-org/-/boards/5772459?milestone_title=Started&label_name%5B%5D=group%3A%3Asecurity%20policies) to assign proper label to issues. You can ignore issues created for UX team or feature flag rollout issues.
1. Go to [Bugs Board](https://gitlab.com/groups/gitlab-org/-/boards/5591377?label_name%5B%5D=type%3A%3Abug&label_name%5B%5D=group%3A%3Asecurity%20policies) to see if there are any bugs that should be handled in planned milestone based on their severity and priority labels.

## Getting started

To automatically create environment and run this script locally you have to:
* install and configure [GitLab CLI](https://gitlab.com/gitlab-org/cli#installation),
* install Ruby (2.7+)

Then you can execute this script by running:
```shell
$ ruby generate-planning.rb MILESTONE SECTION
```

For example:
* `ruby generate-planning 17.1 backend` will generate content needed for backend section using issues scheduled for 17.1
* `ruby generate-planning 17.2 frontend` will generate content needed for frontend section using issues scheduled for 17.2
* `ruby generate-planning 17.3 ` will generate content needed for both backend and frontend section using issues scheduled for 17.3
