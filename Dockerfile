FROM ruby:3.2-alpine

RUN apk add --update --no-cache glab

RUN mkdir /gitlab
WORKDIR /gitlab

COPY generate-planning.rb ./

SHELL ["/bin/bash", "-c"]
CMD ["ruby", "generate-planning.rb"]

